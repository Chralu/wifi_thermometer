# This file is executed on every boot (including wake-boot from deepsleep)
from utils import list_probes, read_temp, start_deepsleep, random, http_get, switch_led, start_pulse_led, do_connect, stop_pulse_led
import config
import time

switch_led(True)
led = start_pulse_led(5, 512)
do_connect(config.WIFI_SSID, config.WIFI_PASSWORD)
stop_pulse_led(led)

led = start_pulse_led(10, 512)
probes = list_probes(config.ONEWIRE_PIN)
print("Found %d probes." % len(probes))
if (len(probes) > 0):
    temperature = read_temp(config.ONEWIRE_PIN, probes[0])
    print("Temperature : %d°C." % temperature)
    http_get("https://api.thingspeak.com/update.json?api_key={}&field1={}".format(config.THINGSPEAK_API_KEY, temperature))
stop_pulse_led(led)

if config.DEEPSLEEP_TIMER != 0:
    start_deepsleep(config.DEEPSLEEP_TIMER)