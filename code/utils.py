def do_connect(ssid, password):
    import network
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print('connecting to network...')
        sta_if.active(True)
        sta_if.connect(ssid, password)
        while not sta_if.isconnected():
            pass
    print('network config:', sta_if.ifconfig())

def do_disconnect():
    import network
    sta_if = network.WLAN(network.STA_IF)
    sta_if.active(False)

def start_pulse_led(frequency, duty):
    import machine
    led = machine.PWM(machine.Pin(2), freq=frequency)
    led.duty(duty)
    return led

def stop_pulse_led(led):
    led.deinit()    

def switch_led(value):
    import machine
    pin = machine.Pin(2, machine.Pin.OUT)
    if value == True:
        pin.off()  #allume la LED"	
    else:
        pin.on()

def start_deepsleep(duration):
    import machine
    # Durée de sommeil
    rtc = machine.RTC()
    rtc.irq(trigger=rtc.ALARM0, wake=machine.DEEPSLEEP)
    rtc.alarm(rtc.ALARM0, duration * 1000)
    print("Now we are going to deep sleep!")
    machine.deepsleep()

def random(max):
    import uos
    return int.from_bytes(uos.urandom(4), 'big')%max

def http_get(url):
    import socket
    _, _, host, path = url.split('/', 3)
    addr = socket.getaddrinfo(host, 80)[0][-1]
    s = socket.socket()
    s.connect(addr)
    s.send(bytes('GET /%s HTTP/1.0\r\nHost: %s\r\n\r\n' % (path, host), 'utf8'))
    while True:
        data = s.recv(100)
        if data:
            print(str(data, 'utf8'), end='')
        else:
            break
    s.close()

def list_probes(onewirePin):
    import machine
    import onewire, ds18x20

    temperatures = {}

    dat = machine.Pin(onewirePin)

    # create the onewire object
    ds = ds18x20.DS18X20(onewire.OneWire(dat))

    # scan for devices on the bus
    return ds.scan()


def read_temp(onewirePin,probeId):
    import time
    import machine
    import onewire, ds18x20
    
    dat = machine.Pin(onewirePin)

    # create the onewire object
    ds = ds18x20.DS18X20(onewire.OneWire(dat))

    ds.convert_temp()
    time.sleep_ms(750)
    return ds.read_temp(probeId)
