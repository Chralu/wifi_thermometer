#!/usr/bin/env bash
source $(dirname "$0")/../.env

if [ $# -ne 1 ]
then 
    echo "Usage : $0 [GET|PUT]"
    exit 1
fi

ELM_DEVICE_NAME=$(basename "${ELM_DEVICE}")

echo $ELM_DEVICE_NAME
cd code
case "$1" in
    PUT)
        mpfshell -n -o $ELM_DEVICE_NAME -c mput .*\.py
        ;;
    GET)
        mpfshell -n -o $ELM_DEVICE_NAME -c mget .*\.py
        ;;
esac
