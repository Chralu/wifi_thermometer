#!/usr/bin/env bash


echo "Make sure your ESP is NOT plugged to USB port."
read -p "[Press Enter to continue]" </dev/tty
echo ''
usbDevicesBefore=`ls /dev/ttyUSB* 2> /dev/null ;ls /dev/tty.usbserial-* 2> /dev/null`

echo "Now, plug your ESP to USB port."
read -p "[Press Enter to continue]" </dev/tty
echo ''
usbDevicesAfter=`ls /dev/ttyUSB* 2> /dev/null ;ls /dev/tty.usbserial-* 2> /dev/null`


newDevices=(`echo ${usbDevicesBefore[@]} ${usbDevicesAfter[@]} | tr ' ' '\n' | sort | uniq -u`)
if [ ${#newDevices[@]} -ne 1 ] 
then
    echo "Unable to detect ESP..."
    exit 1
fi

echo "Found ESP device : ${newDevices[0]}"
echo 'Writing `.env` config file ...'
echo "ELM_DEVICE=\"${newDevices[0]}\" # ELM USB device path." > .env
echo "ELM_BAUD_RATE=115200" >> .env
