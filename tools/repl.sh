#!/usr/bin/env bash
source $(dirname "$0")/../.env

ELM_DEVICE_NAME=$(basename "${ELM_DEVICE}")

mpfshell -n -o $ELM_DEVICE_NAME -c repl
