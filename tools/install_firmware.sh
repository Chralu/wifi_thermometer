#!/usr/bin/env bash
source $(dirname "$0")/../.env

function display_usage {
    echo "Usage : $0 <board_model> <relative_path_to_firmware>"
    echo ""
    echo "    Board models : "
    echo "        Wemos D1 Mini Pro v1.0.0  => D1_MINI_PRO_100"
    echo "        Other                     => OTHER"
}

if [ $# -ne 2 ]
then 
    display_usage
    exit 1
fi


if [ ! -f "$2" ]
then
    echo "Invalid firmware file : '$2'"
    echo ''
    display_usage
    exit 1
fi
FILE_PATH=$(pwd)/$2

case "$1" in
"OTHER")
    esptool.py --port $ELM_DEVICE --baud $ELM_BAUD_RATE erase_flash
    esptool.py --port $ELM_DEVICE --baud $ELM_BAUD_RATE write_flash -fs detect 0 "${FILE_PATH}"
    ;;
"D1_MINI_PRO_100")
    esptool.py --port $ELM_DEVICE --baud $ELM_BAUD_RATE erase_flash
    esptool.py --port $ELM_DEVICE --baud $ELM_BAUD_RATE write_flash -fm dio -fs 4MB 0 "${FILE_PATH}"
    ;;
*)
    echo "Unrecognized board model \"$1\""
    ;;
esac
