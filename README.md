# Thermometer over IP
That device will periodically probe temperature, then post it on your Thingspeak channel.

## Hardware
- ESP8266 (micro controller with WiFi)
- LM35 (temperature sensor)

## Preparation
### Tools installation

> ⚠️ Prequisite : you must install **Python3** first.

```sh
$ pip install esptool
$ pip install mpfshell
```

### Tools setup

> The `setup` script will identify your ELM and write configuration in the `.env` file.

```sh
$ tools/setup.sh
```

### [Optional] Communication via REPL (serial terminal)

```
$ tools/repl.sh
```

## Setup device
### uPython firmware flash

```
$ tools/install_firmware.sh firmware/esp8266-20190125-v1.10.bin
```

### Setup wifi credentials
Edit `code/config.py` with :
  - your WiFi settings
  - your Thingspeak API Key

### Flash software

```sh
$ tools/sync_code.sh
```

## Documentation
- [MicroPython](http://docs.micropython.org/en/latest/esp8266/esp8266/tutorial/)

## Stockage/affichage des courbes
- [Affichage](https://thingspeak.com/channels/481258/private_show)
- [Config](https://thingspeak.com/channels/481258/api_keys)
- [API](https://fr.mathworks.com/help/thingspeak/rest-api.html)

### Poster nouvelle temperature
https://api.thingspeak.com/update.json?api_key=UXTMM9Y8ZAVAI7L5&field1=12
